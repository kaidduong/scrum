CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE "user" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "avatar" character varying(255),
  "displayName" character varying(128) NOT NULL,
  "active" boolean NOT NULL DEFAULT false,
  "email" character varying NOT NULL,
  "password" character varying NOT NULL,
  "birthDate" TIMESTAMP,
  CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"),
  CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"));\

CREATE TABLE "news" (
   "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "userid" uuid NOT NULL ,
  "title" character varying(255),
  "content" character varying(500) NOT NULL,
  "subject" character varying(100),
  "phone" character varying NOT NULL,
  "address" character varying NOT NULL,
  "tuitionPerHour" INTEGER NOT NULL,
  "addmissionFee" INTEGER  NOT NULL,
  "grade" character varying NOT NULL,
  "status" boolean NOT NULL DEFAULT false,
  CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"));

import { Router } from "https://deno.land/x/oak/mod.ts";
import db from "./db.ts";

const router = new Router();

router
  .get("/newss", async (context) => {
    try {
      
      const result = await db.query({
              text: 'SELECT * from "newss"'
    } );

    console.log(result.rowsOfObjects());
    context.response.body = result.rowsOfObjects();

  }catch (err) {
      console.log(err);
      context.throw(err);
    }
  })
  // .get("/news/:id", async (context) => {
  //   if (context.params && context.params.id) {
  //     const result = await db.query({
  //       text: 'SELECT * from "news" WHERE id = $1;',
  //       args: [context.params.id],
  //     });
  //     context.response.body = result.rowsOfObjects()[0];
  //   }
  // })
  .post("/newss", async (context) => {
    if (context.request.hasBody) {
      try {
        const body = await context.request.body({
          contentTypes: {
            text: ["application/json"],
          },
        });
        
        const data = body.value;
        console.log(data);
        const result = await db.query({
          text:
            'INSERT INTO "newss" ("userid",content,title, grade, "tuitionPerHour", "addmissionFee", address,"subject",phone,status) VALUES ($1, $2, $3,$4, $5, $6,$7,$8, $9,$10) RETURNING *;',
          args: [data.userid, data.content, data.title, data.grade, data.tuition,data.fee, data.address,data.subject, data.phone,data.status],
        });
        context.response.body = result.rowsOfObjects()[0];
      } catch (err) {
        console.log(err);
        context.throw(err);
      }
    }
  })
export default router;

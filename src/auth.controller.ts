// import { serve } from "https://deno.land/std/http/server.ts"
// import { validateJwt } from "https://deno.land/x/djwt/validate.ts"
// import { makeJwt, setExpiration, Jose, Payload } from "https://deno.land/x/djwt/create.ts"
// import { Router } from "https://deno.land/x/oak/mod.ts";
// import db from "./db.ts";

// const key = "18e69f2ba53986d08400c6a3ac2cad87f2034fe8cb277c9f2c5a3bd4c986f18a"

// const header: Jose = {
//   alg: "HS256",
//   typ: "JWT",
// }
// const router = new Router();

// router
// .post("/login", async (context) => {
//   if (context.request.hasBody) {
//     try {
//       const body = await context.request.body({
//         contentTypes: {
//           text: ["application/json"],
//         },
//       });
//       const data = body.value;
      
//       const result = await db.query({
//            text: 'SELECT * from "news" WHERE "displayName" = $1;',
//            args: [data.username],
//             });
//         console.log("result", result.rows[0]);

//         const payload: Payload = {
//           iss: "joe",
//           exp: setExpiration(new Date().getTime() + 60000),
//           username: data.username,
//         }
//        const token =makeJwt({ header, payload, key });
//        console.log("token", token);
//        context.response.body = {
//          token: token,
//          currentUser: {
//            userid: result.rows[0].id,
//            username: result.rows[0].displayName, 
//            email: result.rows[0].email
//          }
//        }
      
//     } catch (err) {
//       console.log(err);
//       context.throw(err);
//     }
//   }
// })

// export default router;

import { Router } from "https://deno.land/x/oak/mod.ts";
import db from "./db.ts";

const router = new Router();

router
.post("/login", async (context) => {
  if (context.request.hasBody) {
    try {
      const body = await context.request.body({
        contentTypes: {
          text: ["application/json"],
        },
      });
      const data = body.value;
      const result = await db.query({
        text: 'SELECT * from "user" WHERE "email" = $1;',
        args: [data.email],
      });
      console.log(result.rowsOfObjects()[0]);
      if(data.email == result.rowsOfObjects()[0].password){
      context.response.body = result.rowsOfObjects()[0];
      }
      // if(data.email == result.rowsOfObjects()[0].password){
      //   context.response.body = result.rowsOfObjects()[0];
      //   }
      // else{ 
      //   context.response.body= {userid : "null"}
      // }
      context.response.body = result.rowsOfObjects()[0];
    } catch (err) {
      console.log(err);
      context.throw(err);
    }
  }
})
.post("/logout", async (context) => {
  if (context.request.hasBody) {
    try {
      const body = await context.request.body({
        contentTypes: {
          text: ["application/json"],
        },
      });
      context.response.body = {status: 200}
    } catch (err) {
      console.log(err);
      context.throw(err);
    }
  }
})

export default router;
import { Application } from "https://deno.land/x/oak/mod.ts";
import UserController from './user.controller.ts';
import NewsController from './new.controller.ts';
import AuthController from './auth.controller.ts';
import { oakCors } from "https://deno.land/x/cors/mod.ts";
const app = new Application();

// Logger
app.use(async (ctx, next) => {
  await next();
  const rt = ctx.response.headers.get("X-Response-Time");
  console.log(`${ctx.request.method} ${ctx.request.url} - ${rt}`);
});

// Timing
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.response.headers.set("X-Response-Time", `${ms}ms`);
  
});
app.use(
  oakCors({
    origin: /^.+localhost:(4200)$/,
    // optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  }),
);
app.use(UserController.routes());
app.use(NewsController.routes());
app.use(AuthController.routes());
app.use(AuthController.allowedMethods());
app.use(UserController.allowedMethods());
app.use(NewsController.allowedMethods());
console.log(`🚀 Server started at port 8000`);
app.listen({ port: 8000 });

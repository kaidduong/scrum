import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {Roles} from '../shared/constants/roles';

@Injectable({
  providedIn: 'root'
})
export class CheckRoleGuard implements CanActivate{
  
  constructor(private router: Router){

  }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
     if(localStorage.getItem('auth_token') == null){
       this.router.navigate(['']);
     }
     let currentUser = localStorage.getItem('currentUser')
     let canActivate= false;
     if(localStorage.getItem('currentUser') != null){
       if(state.url === "/accounts") {
        canActivate = JSON.parse(currentUser).roles.filter (x => x === Roles.admin).length > 0
       } else {
        canActivate =  JSON.parse(currentUser).roles.filter (x => x === Roles.admin || x === Roles.tutor).length > 0
       }
     }
    if(canActivate){
      return true
    } else {
      this.router.navigate(['/403']);
      return false;
    }
  }
}

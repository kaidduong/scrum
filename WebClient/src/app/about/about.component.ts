import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { NewsService } from '../shared/services/news.service';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  public imagesUrl;
  newsList;
  constructor(private http: HttpClient, private newsservice:NewsService) { }

  ngOnInit() {
    this.imagesUrl = ['/../../assets/images/gsa.jpg','../../assets/images/gsb.jpg','../../assets/images/gsc.jpg'];
    this.GetNewsData();
  }
  GetNewsData() {
    this.newsservice.GetAllNews().subscribe((data: any) => {
      this.newsList = data.body;
      console.log(data);
    });
  }

}

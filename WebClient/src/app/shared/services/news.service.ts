import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) {
  }
  GetAllNews() {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(environment.apiBaseURI + 'newss',{headers: headers, observe: 'response', responseType: 'json'});
  }
  createNews(data: any) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(environment.apiBaseURI + 'newss',data,{headers: headers, observe: 'response', responseType: 'json'});
  }
}


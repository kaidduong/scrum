import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as _ from 'underscore';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
  })
  export class UserService {
    isRole: boolean = false;
  
    constructor(private router: Router, private http: HttpClient) { }
    
    // hàm gửi Token google user api đến server sử lý
    googleLogin(idToken: string) {
      var headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });
      var payload = {token: idToken};
      return this.http.post(environment.apiBaseURI + 'auth/login', JSON.stringify(payload),{headers}).toPromise();
    }
    login(data: any) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(environment.apiBaseURI + 'login',data,{headers: headers, observe: 'response', responseType: 'json'});
  }
  register(data: any) {
    var headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(environment.apiBaseURI + 'users',data,{headers: headers, observe: 'response', responseType: 'json'});
  }
  }
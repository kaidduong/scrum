import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {AuthService,GoogleLoginProvider, SocialLoginModule} from 'angularx-social-login';

import { UserService } from 'src/app/shared/services/user.service';
import { MessageService } from 'src/app/shared/services/message.service';
@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css'],
  providers: [SocialLoginModule, AuthService]
})
export class UserLoginComponent implements OnInit {


  jwt : string;
  
  loginForm: FormGroup;
  submitted = false;
  constructor(private socialAuthService : AuthService,
             private  userService: UserService,
             private router: Router, 
             private messageService : MessageService,
             private formBuilder: FormBuilder
             ){}

             ngOnInit() {
              this.loginForm = this.formBuilder.group({
                  email: ['', [Validators.required, Validators.email]],
                  password: ['', [Validators.required, Validators.minLength(6)]]
              });
          }
          get f() { return this.loginForm.controls; }

          onSubmit() {
              this.submitted = true;
      
              // stop here if form is invalid
              if (this.loginForm.invalid) {
                  return;
              }
              this.userService.login(this.loginForm.value).subscribe((res : any)=>{
               // console.log(res)
                localStorage.setItem("currentUser",JSON.stringify(res.body));
                // display form values on success
              alert('You logged in successfully!');
                this.router.navigateByUrl('/news');
              },(err: any)=>{
                alert('Đã có lỗi xảy ra trong quá trình đăng nhập, bạn vui lòng thử lại! ');
              });
              
          }
      

  socialLogin(){
    let socialPlatformProvider;
    socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (res : any)=>{ //res is the data which is returned by Google User Api
        console.log(res);
        this.userService.googleLogin(res.idToken).then((res: any)=>{
          this.jwt = res.token;
          localStorage.setItem("auth_token",res.token);
          localStorage.setItem("currentUser",JSON.stringify(res));
          this.messageService.sendMessage({reloadUser: true});
          this.router.navigateByUrl('/news');
        });

      }
    );
  }
}

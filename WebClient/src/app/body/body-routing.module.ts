import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NgxPrintModule } from 'ngx-print';

import { CheckRoleUser } from '../auth/check-role-user';

import { BodyComponent } from './body.component';

import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', component: BodyComponent, canActivate: [CheckRoleUser], children: [
    { path: 'home', component: HomeComponent},
  ]}
];


@NgModule({
  imports: [RouterModule.forChild(routes),NgxPrintModule],
  exports: [RouterModule]
})

export class BodyRoutingModule { }

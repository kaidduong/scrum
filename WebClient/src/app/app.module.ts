import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";

import { ModalModule } from 'ngx-bootstrap/modal';

import { AuthServiceConfig, GoogleLoginProvider } from 'angularx-social-login';

import { SliderModule } from 'angular-image-slider';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AboutComponent } from './about/about.component';
import { BodyRoutingModule } from './body/body-routing.module';
import { BodyComponent } from './body/body.component';
import { FooterComponent } from './footer/footer.component';
import { NavComponent } from './nav/nav.component';
import { NewsComponent } from './news/news.component';
import { ConfirmDialogComponent } from './body/dialog/dialog.component';
import { HomeComponent } from './body/home/home.component';
import { AuthInterceptor } from './shared/interceptor/auth-interceptor';
import { UserLoginComponent } from './user/user-login/user-login.component';
import { RegistComponent } from './regist/regist.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddNewsComponent } from './add-news/add-news.component';
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("659589669275-bkeko1ise012qmsrbl5h6663s99lf9es.apps.googleusercontent.com")
        }
      ]
  );
  return config;
}
@NgModule({
  declarations: [
    AppComponent,
    BodyComponent,
    NavComponent,
    FooterComponent,
    UserLoginComponent, 
    ConfirmDialogComponent,
    HomeComponent,
    RegistComponent,
    NewsComponent,
    AboutComponent,
    AddNewsComponent
  ],
  entryComponents: [
    ConfirmDialogComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BodyRoutingModule,
    BrowserAnimationsModule,
    SliderModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    ModalModule.forRoot(),
    
  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: getAuthServiceConfigs,
  },
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
],
  bootstrap: [AppComponent]
})
export class AppModule { }
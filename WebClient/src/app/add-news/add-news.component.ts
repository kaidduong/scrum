import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
//import { REACTIVE_FORM_DIRECTIVES } from '@angular/forms';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { first } from 'rxjs/operators';
import { NewsService } from 'src/app/shared/services/news.service';

@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.css']
})

export class AddNewsComponent implements OnInit {
  submitted = false;
  data : any;
  user: any;
  formNews = new FormGroup({
    title: new FormControl(''),
    phone: new FormControl(''),
    address: new FormControl(''),
    content: new FormControl(''),
    subject: new FormControl(''),
    grade: new FormControl(''),
    fee: new FormControl(''),
    tuition: new FormControl('')
  });

  constructor(
        private route: ActivatedRoute,
        private router: Router,
        private newService: NewsService
  ) { }

  ngOnInit() {}

   // convenience getter for easy access to form fields
  get f() { return this.formNews.controls; }

  onSubmit(): void {
    this.user = localStorage.getItem("currentUser");
    console.log(this.user)
    this.data = {
      title: this.formNews.value.title,
      content: this.formNews.value.content,
      tuition : this.formNews.value.tuition,
      fee : this.formNews.value.fee,
      userid: "87a9e9ce-b89b-4e76-b20b-5d813d555c2f",
      grade: this.formNews.value.grade,
       phone: "03748951",
        address: "âu cơ đà nẵng",  
        subject: "toán",
        status: false,
    };
    console.log(this.data);
    this.newService.createNews(this.data).subscribe((res : any)=>{
      // console.log(res)
       // display form values on success
     alert('Bạn đã tạo bài đăng thành công!');
       this.router.navigateByUrl('/news');
     },(err: any)=>{
       alert('Đã có lỗi xảy ra trong quá trình tạo bài đăng, bạn vui lòng thử lại! ');
     });
     // {first: 'Nancy', last: 'Drew'}
    // var obj = JSON.parse(localStorage.getItem('currentUser'));

    // console.log("user id: " + obj.id);

    // this.newService.createNew(obj.id,
    //   this.f.title.value,
    //   this.f.content.value,
    //   this.f.subject.value,
    //   this.f.grade.value,
    //   this.f.fee.value,
    //   this.f.tuition.value,
    //   this.f.address.value,
    //   this.f.phone.value,
    //   false)
    //   .pipe(first())
    //   .subscribe(data => console.log(data));



   // this.new.userid = obj.id;
    //title
    // content: string;
    // subject: string;
    // grade: number ;
    // addmissionFee: number;
    // tuitionPerHour: number;
    // address: string;
    // phone: string;
    // status: false;
  }

}

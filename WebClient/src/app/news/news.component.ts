import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { NewsService } from 'src/app/shared/services/news.service';
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  newsList;
  constructor(private http: HttpClient, private newsservice:NewsService,private router: Router, ) { }

  ngOnInit() {
    this.GetNewsData();
  }
  GetNewsData() {
    this.newsservice.GetAllNews().subscribe((data: any) => {
      this.newsList = data.body;
      console.log(data);
    });
  }
  Logout(){
    localStorage.removeItem("auth_token");
    localStorage.removeItem("currentUser");
    this.router.navigateByUrl('/');
  }
}

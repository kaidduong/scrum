import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { RegistComponent } from './regist/regist.component';
import { AuthGuard } from './auth/check-login.guard';
import { NewsComponent } from './news/news.component';
import { UserLoginComponent } from './user/user-login/user-login.component';
import { AddNewsComponent } from './add-news/add-news.component';
const routes: Routes = [
  { path:'login', component: UserLoginComponent,canActivate: [AuthGuard]},//canactive
  { path:'', component: AboutComponent},
  { path:'regist', component: RegistComponent},

  { path:'news', component: NewsComponent},
  { path:'add-news', component: AddNewsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
